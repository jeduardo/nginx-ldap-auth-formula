import testinfra

def test_environment_file_in_place(host):
	env_file = host.file('/etc/default/nginx-ldap-auth')
	assert env_file.user == 'nginx-ldap-auth'
	assert env_file.group == 'nginx-ldap-auth'
	assert 'testtest' in env_file.content

def test_config_file_in_place(host):
	conf_file = host.file('/var/lib/nginx-ldap-auth/code/config.json')
	assert conf_file.user == 'nginx-ldap-auth'
	assert conf_file.group == 'nginx-ldap-auth'
	assert 'Test - Restricted' in conf_file.content

def test_service_is_running_and_enabled(Service):
    svc = Service('nginx-ldap-auth')
    assert svc.is_running
    assert svc.is_enabled
